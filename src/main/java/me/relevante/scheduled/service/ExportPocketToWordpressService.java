package me.relevante.scheduled.service;

import me.relevante.api.NetworkBasicAuthCredentials;
import me.relevante.api.NetworkOAuthCredentials;
import me.relevante.api.PocketApi;
import me.relevante.api.PocketApiImpl;
import me.relevante.api.WordpressApi;
import me.relevante.api.WordpressApiException;
import me.relevante.api.WordpressApiImpl;
import me.relevante.model.PocketItem;
import me.relevante.model.PocketWordpressMapping;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.WordpressCategory;
import me.relevante.model.WordpressFormat;
import me.relevante.model.WordpressHub;
import me.relevante.model.WordpressMedia;
import me.relevante.model.WordpressPost;
import me.relevante.model.WordpressProfile;
import me.relevante.model.WordpressStatus;
import me.relevante.model.WordpressTag;
import me.relevante.network.Pocket;
import me.relevante.network.Wordpress;
import me.relevante.scheduled.model.PocketWordpressItem;
import me.relevante.scheduled.persistence.PocketItemRepo;
import me.relevante.scheduled.persistence.PocketWordpressItemRepo;
import me.relevante.scheduled.persistence.PocketWordpressMappingRepo;
import me.relevante.scheduled.persistence.RelevanteAccountRepo;
import me.relevante.scheduled.persistence.WordpressHubRepo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ExportPocketToWordpressService {

    private static final Logger logger = LoggerFactory.getLogger(ExportPocketToWordpressService.class);
    private static final String POCKET_ALL_ITEMS_TAG = "*";

    private PocketWordpressMappingRepo pocketWordpressMappingRepo;
    private RelevanteAccountRepo relevanteAccountRepo;
    private PocketWordpressItemRepo pocketWordpressItemRepo;
    private WordpressHubRepo wordpressHubRepo;
    private PocketItemRepo pocketItemRepo;

    @Autowired
    public ExportPocketToWordpressService(PocketWordpressMappingRepo pocketWordpressMappingRepo,
                                          RelevanteAccountRepo relevanteAccountRepo,
                                          PocketWordpressItemRepo pocketWordpressItemRepo,
                                          WordpressHubRepo wordpressHubRepo,
                                          PocketItemRepo pocketItemRepo) {
        this.pocketWordpressMappingRepo = pocketWordpressMappingRepo;
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.pocketWordpressItemRepo = pocketWordpressItemRepo;
        this.wordpressHubRepo = wordpressHubRepo;
        this.pocketItemRepo = pocketItemRepo;
    }

    public void exportPocketToWordpress() throws Exception {

        List<PocketWordpressMapping> pocketWordpressMappings = getOneMappingPerHub();
        for (PocketWordpressMapping pocketWordpressMapping : pocketWordpressMappings) {
            RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(pocketWordpressMapping.getRelevanteId());
            NetworkOAuthCredentials<Pocket> pocketCredentials = (NetworkOAuthCredentials<Pocket>) relevanteAccount.getCredentials(Pocket.getInstance());
            NetworkBasicAuthCredentials<Wordpress> wpCredentials = (NetworkBasicAuthCredentials<Wordpress>) relevanteAccount.getCredentials(Wordpress.getInstance());
            exportPocketToWordpressMapping(pocketWordpressMapping, pocketCredentials, wpCredentials);
        }

    }

    private List<PocketWordpressMapping> getOneMappingPerHub() {

        List<PocketWordpressMapping> customerMappings = new ArrayList<>();
        List<PocketWordpressMapping> allMappings = pocketWordpressMappingRepo.findAll();
        Set<String> mappingsByHub = new HashSet<>();
        for (PocketWordpressMapping currentMapping : allMappings) {
            if (mappingsByHub.contains(currentMapping.getWpHubId()))
                continue;
            mappingsByHub.add(currentMapping.getWpHubId());
            customerMappings.add(currentMapping);
        }

        return customerMappings;
    }

    private void exportPocketToWordpressMapping(PocketWordpressMapping mapping,
                                                NetworkOAuthCredentials<Pocket> pocketCredentials,
                                                NetworkBasicAuthCredentials<Wordpress> wpCredentials) throws Exception {
        PocketApi pocketApi = new PocketApiImpl(pocketCredentials.getOAuthConsumerKeyPair(), pocketCredentials.getOAuthAccessTokenPair());
        String pocketTag = mapping.getPocketTag();
        List<PocketItem> pocketItems = (pocketTag.equals(POCKET_ALL_ITEMS_TAG)) ? pocketApi.getItems() : pocketApi.getItemsByTag(pocketTag);
        for (PocketItem pocketItem : pocketItems) {
            if (!pocketItemRepo.exists(pocketItem.getId())) {
                pocketItemRepo.save(pocketItem);
            }
            PocketWordpressItem pocketWordpressItem = pocketWordpressItemRepo.findOneByPocketItemIdAndWordpressHubId(pocketItem.getId(), mapping.getWpHubId());
            if (pocketWordpressItem != null) {
                if (pocketWordpressItem.getCreationTimestamp() == null) {
                    Date date = Date.from(LocalDateTime.now().minusWeeks(2).toInstant(ZoneOffset.UTC));
                    pocketWordpressItem.setCreationTimestamp(date);
                    pocketWordpressItemRepo.save(pocketWordpressItem);
                }
                continue;
            }
            exportPocketItemToWordpress(pocketItem, mapping.getWpHubId(), mapping.getWpCategory(), wpCredentials);
            pocketWordpressItem = new PocketWordpressItem(pocketItem.getId(), mapping.getWpHubId(), new Date());
            pocketWordpressItemRepo.save(pocketWordpressItem);
            pocketItemRepo.save(pocketItem);
        }

    }

    private void exportPocketItemToWordpress(PocketItem pocketItem,
                                             String wpHubId,
                                             String wpCategoryName,
                                             NetworkBasicAuthCredentials<Wordpress> wpCredentials) throws Exception {
        WordpressHub wpHub = wordpressHubRepo.findOne(wpHubId);
        WordpressApi wpApi = new WordpressApiImpl(wpHub.getUrl(), wpCredentials.getUsername(), wpCredentials.getPassword());
        WordpressCategory wpCategory = obtainWpCategory(wpApi, wpCategoryName);
        WordpressProfile wpProfile = wpApi.getUserData();
        WordpressPost wpPost = createWordpressPostFromPocketItem(pocketItem, wpCategory, wpProfile.getLocalId());
        if (pocketItem.hasImage()) {
            try {
                WordpressMedia wpImage = wpApi.uploadImage(pocketItem.getImages().get(0).getSrc());
                wpPost.setFeaturedMediaId(wpImage.getId());
            } catch (WordpressApiException e) {
                logger.error("Error", e);
            }
        }
        wpApi.createPost(wpPost);
    }

    private WordpressCategory obtainWpCategory(WordpressApi wpApi,
                                               String categoryName) {
        List<WordpressCategory> categories = wpApi.getCategories();
        for (WordpressCategory category : categories) {
            if (category.getName().equals(categoryName)) {
                return category;
            }
        }
        return null;
    }

    private WordpressTag obtainWpTag(WordpressApi wpApi,
                                     String tagName) {
        List<WordpressTag> tags = wpApi.getTags();
        for (WordpressTag tag : tags) {
            if (tag.getName().equals(tagName)) {
                return tag;
            }
        }
        return null;
    }

    private WordpressPost createWordpressPostFromPocketItem(PocketItem pocketItem,
                                                            WordpressCategory wpCategory,
                                                            Long wpUserId) {
        WordpressPost wpPost = new WordpressPost();
        wpPost.setAuthor(wpUserId);
        wpPost.setTitle(StringUtils.isBlank(pocketItem.getGivenTitle()) ? pocketItem.getResolvedTitle() : pocketItem.getGivenTitle());
        wpPost.setExcerpt(pocketItem.getExcerpt());
        wpPost.setFormat(WordpressFormat.STANDARD);
        String content = composeWordpressPostContent(pocketItem);
        wpPost.setContent(content);
        Date date = (pocketItem.getCreationTimestamp() != null ? pocketItem.getCreationTimestamp() : new Date());
        LocalDateTime localDateTimeGmt = LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);
        wpPost.setDate(localDateTimeGmt);
        wpPost.setDateGmt(localDateTimeGmt);
        wpPost.setExcerpt(pocketItem.getExcerpt());
        if (wpCategory != null) {
            wpPost.getCategoryIds().add(wpCategory.getId());
        }
        wpPost.setOpenedToComments(true);
        wpPost.setOpenedToPing(true);
        wpPost.setSticky(false);
        wpPost.setStatus(WordpressStatus.PUBLISH);
        return wpPost;
    }

    private String composeWordpressPostContent(PocketItem pocketItem) {
        String content = "";
//        if (pocketItem.hasImage()) {
//            PocketImage pocketImage = pocketItem.getImages().get(0);
//            content += "<p>"
//                    + "<img src=\"" + pocketImage.getSrc() + "\" alt=\"" + pocketImage.getCaption() + "\" height=\"" + pocketImage.getHeight() + "\" width=\"" + pocketImage.getWidth() + "\">"
//                    + "</p>";
//        }
        content += "<p>";
        if (StringUtils.isBlank(pocketItem.getExcerpt())) {
            content += "<a href=\"" + pocketItem.getResolvedUrl() + "\" target=\"_blank\">Read content</a>";
        } else {
            content += pocketItem.getExcerpt() + "... "
                    + "<a href=\"" + pocketItem.getResolvedUrl() + "\" target=\"_blank\">continue reading</a>";
        }
        content += "</p>"
                + "<p>"
                + "via <a href=\"https://www.relevante.me\" target=\"_blank\">relevante.me</a>"
                + "</p>";
        return content;
    }

}