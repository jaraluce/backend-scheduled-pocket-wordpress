package me.relevante.scheduled.persistence;

import me.relevante.model.PocketWordpressMapping;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PocketWordpressMappingRepo extends MongoRepository<PocketWordpressMapping, ObjectId> {
}
