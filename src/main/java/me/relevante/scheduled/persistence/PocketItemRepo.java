package me.relevante.scheduled.persistence;

import me.relevante.model.PocketItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PocketItemRepo extends MongoRepository<PocketItem, String> {
}
