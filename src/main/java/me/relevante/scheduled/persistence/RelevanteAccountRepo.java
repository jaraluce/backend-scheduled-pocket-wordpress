package me.relevante.scheduled.persistence;

import me.relevante.model.RelevanteAccount;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelevanteAccountRepo extends MongoRepository<RelevanteAccount, String> {
}
