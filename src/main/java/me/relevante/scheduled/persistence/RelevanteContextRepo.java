package me.relevante.scheduled.persistence;

import me.relevante.model.RelevanteContext;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelevanteContextRepo extends MongoRepository<RelevanteContext, String> {
}
