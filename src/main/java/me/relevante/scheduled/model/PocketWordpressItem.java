package me.relevante.scheduled.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by daniel-ibanez on 5/10/16.
 */
@Document(collection = "PocketWordpressItem")
public class PocketWordpressItem {

    @Id
    private String id;
    private String pocketItemId;
    private String wordpressHubId;
    private Date creationTimestamp;

    public PocketWordpressItem(String pocketItemId,
                               String wordpressHubId,
                               Date creationTimestamp) {
        this.pocketItemId = pocketItemId;
        this.wordpressHubId = wordpressHubId;
        this.creationTimestamp = creationTimestamp;
    }

    public String getId() {
        return id;
    }

    public String getPocketItemId() {
        return pocketItemId;
    }

    public String getWordpressHubId() {
        return wordpressHubId;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }
}
