package me.relevante.scheduled.task;

import me.relevante.scheduled.service.ExportPocketToWordpressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ExportPocketToWordpressTask {

    private static final Logger logger = LoggerFactory.getLogger(ExportPocketToWordpressTask.class);
    private static final int MINUTE_IN_MILLISECONDS = 60 * 1000;

    @Autowired private ExportPocketToWordpressService exportService;

    @Scheduled(fixedRate = 10 * MINUTE_IN_MILLISECONDS)
    public void exportPocketToWordpress() throws Exception {
        exportService.exportPocketToWordpress();
    }

}